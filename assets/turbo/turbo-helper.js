import { Modal } from 'bootstrap'
import * as Turbo from '@hotwired/turbo'

const TurboHelper = class {
  constructor () {
    document.addEventListener('turbo:before-cache', () => {
      this.closeModal()
      this.closeSweetAlert()
      this.reenableSubmitButtons()
    })

    document.addEventListener('turbo:visit', () => {
      // fade out the old body
      document.body.classList.add('turbo-loading')
    })

    document.addEventListener('turbo:submit-start', event => {
      const submitter = event.detail.formSubmission.submitter
      submitter.toggleAttribute('disabled', true)
      submitter.classList.add('turbo-submit-disabled')
    })

    document.addEventListener('turbo:before-fetch-request', event => this.beforeFetchRequest(event))

    document.addEventListener('turbo:before-fetch-response', event => this.beforeFetchResponse(event))

    this.initializeTransitions()
  }

  beforeFetchRequest (event) {
    const frameId = event.detail.fetchOptions.headers['Turbo-Frame']
    if (!frameId) {
      return
    }
    const frame = document.getElementById(frameId)
    if (!frame || !frame.dataset.turboFormRedirect) {
      return
    }
    event.detail.fetchOptions.headers['Turbo-Frame-Redirect'] = 1
  }

  beforeFetchResponse (event) {
    const fetchResponse = event.detail.fetchResponse
    const redirectLocation = fetchResponse.response.headers.get('Turbo-Location')
    if (!redirectLocation) {
      return
    }
    event.preventDefault()
    Turbo.clearCache()
    Turbo.visit(redirectLocation)
  }

  closeModal () {
    if (!document.body.classList.contains('modal-open')) {
      return
    }
    const modalEl = document.querySelector('.modal')
    const modal = Modal.getInstance(modalEl)
    modalEl.classList.remove('fade')
    modal._backdrop._config.isAnimated = false
    modal.hide()
    modal.dispose()
  }

  closeSweetAlert () {
    // Internal way to see if sweetalert2 has been imported yet
    if (!__webpack_modules__[require.resolveWeak('sweetalert2')]) {
      return
    }
    // Because we know it's been imported, this will run synchronously
    import('sweetalert2').then(Swal => {
      if (!Swal.default.isVisible()) {
        return
      }
      Swal.default.getPopup().style.animationDuration = '0ms'
      Swal.default.close()
    })
  }

  initializeTransitions () {
    document.addEventListener('turbo:before-render', event => {
      if (this.isPreviewRendered()) {
        event.detail.newBody.classList.remove('turbo-loading')
        requestAnimationFrame(() => {
          event.detail.newBody.classList.add('turbo-loading')
        })
      } else {
        const isRestoration = event.detail.newBody.classList.contains('turbo-loading')
        if (isRestoration) {
          // this is a restoration (back button). Remove the class
          // so it simply starts with full opacity
          event.detail.newBody.classList.remove('turbo-loading')
          return
        }
        // when we are *about* to render, start us faded out
        event.detail.newBody.classList.add('turbo-loading')
      }
    })
    document.addEventListener('turbo:render', () => {
      if (this.isPreviewRendered()) {
        return
      }
      // after rendering, we first allow the turbo-loading class to set the low opacity
      // THEN, one frame later, we remove the turbo-loading class, which allows the fade in
      requestAnimationFrame(() => {
        document.body.classList.remove('turbo-loading')
      })
    })
  }

  isPreviewRendered () {
    return document.documentElement.hasAttribute('data-turbo-preview')
  }

  reenableSubmitButtons () {
    document.querySelectorAll('.turbo-submit-disabled').forEach(button => {
      button.toggleAttribute('disabled', false)
      button.classList.remove('turbo-submit-disabled')
    })
  }
}

export default new TurboHelper()
